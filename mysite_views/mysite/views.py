#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.http import HttpResponse
import datetime

def hola(request):
    return HttpResponse('Hola Mundo')

def ejemplo_html(request):
    html = """
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http­equiv="content­type" content="text/html; charset=utf­8">
    <meta name="robots" content="NONE,NOARCHIVE">
    <title>Hola mundo</title>
    <style type="text/css">
        html * { padding:0; margin:0; }
        body * { padding:10px 20px; }
        body * * { padding:0; }
        body { font:small sans­serif; }
        body>div { border­bottom:1px solid #ddd; }
        h1 { font­weight:normal; }
        #summary { background: #e0ebff; }
    </style>
</head>
<body>
    <div id="summary">
      <h1>¡Hola Mundo!</h1>
    </div>
</body>
</html>
"""
    return HttpResponse(html)

def fecha_actual(request):
    html = """
<html>
    <head>
        <meta http-equiv="refresh" content="1"/>
    </head>
    <body>
        <h1>Fecha:</h1><h3>%s<h/3>
    </body>
</html>""" % datetime.datetime.now()
    return HttpResponse(html)
